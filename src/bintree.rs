use std::boxed::Box;
use std::cmp::{max, PartialOrd};

#[derive(Debug)]
pub struct BinTree<K: PartialOrd, D> {
    root: BinTreeNodeReference<K, D>,
}

enum DeleteResult<K, D>
where
    K: PartialOrd,
{
    NotFoundOrSkip,
    Deleted(BinTreeNodeReference<K, D>),
}

impl<K, D> BinTree<K, D>
where
    K: PartialOrd,
{
    /// Creates new binary tree without elements
    pub fn new() -> Self {
        BinTree { root: None }
    }

    /// Searches for value in binary tree with specified key
    ///
    /// # Returns
    /// * `Option::None` if no value with specified key have found
    /// * `Option::Some(&D)` if data found, a value stored in connected enum field
    pub fn find(&self, key: K) -> Option<&D> {
        match &self.root {
            None => None,
            Some(r) => r.find(key),
        }
    }

    /// Inserts element in binary tree
    /// # Returns
    /// boolean value: true if a value inserted; false otherwise
    ///
    /// # Arguments:
    /// * `key` - search key for a value
    /// * `value` - Data stored on specified key
    ///
    pub fn insert(&mut self, key: K, value: D) -> bool {
        let node = BinTreeNode::new(key, value);
        let box_node = Box::new(node);

        let mut result = true;
        match &mut self.root {
            None => self.root = Some(box_node),
            Some(n) => result = n.insert(box_node),
        }

        result
    }

    /// Removes element with specified key
    pub fn remove(&mut self, key: K) {
        let mut result = DeleteResult::NotFoundOrSkip;
        if let Some(root) = &mut self.root {
            result = root.remove(key);
        }

        if let DeleteResult::Deleted(n) = result {
            self.root = n;
        }
    }

    pub fn height(&self) -> i32 {
        match &self.root {
            None => 0,
            Some(n) => n.height(),
        }
    }
}

#[derive(Debug)]
struct BinTreeNode<K: PartialOrd, D> {
    left_node: BinTreeNodeReference<K, D>,
    right_node: BinTreeNodeReference<K, D>,

    key: K,
    data: D,
}

type BinTreeNodeReference<K, D> = Option<Box<BinTreeNode<K, D>>>;

impl<K: PartialOrd, D> BinTreeNode<K, D> {
    pub fn new(key: K, data: D) -> Self {
        BinTreeNode {
            left_node: None,
            right_node: None,

            key,
            data,
        }
    }

    pub fn find(&self, key: K) -> Option<&D> {
        if key < self.key {
            match &self.left_node {
                None => None,
                Some(n) => n.find(key),
            }
        } else if key > self.key {
            match &self.right_node {
                None => None,
                Some(n) => n.find(key),
            }
        } else {
            Some(&self.data)
        }
    }

    pub fn insert(&mut self, node: Box<BinTreeNode<K, D>>) -> bool {
        let key = &node.key;
        if key < &self.key {
            let mut result = true;
            match &mut self.left_node {
                None => self.left_node = Some(node),
                Some(n) => result = n.insert(node),
            };
            result
        } else if key > &self.key {
            let mut result = true;
            match &mut self.right_node {
                None => self.right_node = Some(node),
                Some(n) => result = n.insert(node),
            }
            result
        } else {
            false
        }
    }

    pub fn leaf(&self) -> bool {
        if let None = &self.left_node {
            if let None = &self.right_node {
                return true;
            }
        }
        return false;
    }

    pub fn remove(&mut self, key: K) -> DeleteResult<K, D> {
        let mut result = DeleteResult::NotFoundOrSkip;
        // Move to left side
        if key < self.key {
            if let Some(n) = &mut self.left_node {
                result = n.remove(key);
            }

            if let DeleteResult::Deleted(n) = result {
                self.left_node = n;
                result = DeleteResult::NotFoundOrSkip;
            }
        }
        // Move to right side
        else if key > self.key {
            if let Some(n) = &mut self.right_node {
                result = n.remove(key);
            }

            if let DeleteResult::Deleted(n) = result {
                self.right_node = n;
                result = DeleteResult::NotFoundOrSkip;
            }
        }
        // Remove
        else {
            result = self.remove_node();
        }

        return result;
    }

    fn remove_node(&mut self) -> DeleteResult<K, D> {
        if self.leaf() {
            return DeleteResult::Deleted(None);
        } else if let Some(_) = &mut self.left_node {
            return self.remove_node_left();
        } else {
            return self.remove_node_right();
        }
    }

    fn remove_node_left(&mut self) -> DeleteResult<K, D> {
        if let Some(left_node) = &mut self.left_node {
            if let None = &mut left_node.right_node {
                left_node.right_node = self.right_node.take();
                return DeleteResult::Deleted(self.left_node.take());
            }
            let mut current_node = left_node;
            while let Some(mut next_node) = current_node.right_node.take() {
                if let None = next_node.right_node {
                    let result = next_node.remove_node();
                    if let DeleteResult::Deleted(n) = result {
                        current_node.right_node = n;
                        next_node.left_node = self.left_node.take();
                        next_node.right_node = self.right_node.take();
                        return DeleteResult::Deleted(Some(next_node));
                    }

                    break;
                }
                current_node.right_node = Some(next_node); // Return ownership
                current_node = current_node.right_node.as_mut().unwrap(); // Move to next right element
            }
        }
        return DeleteResult::NotFoundOrSkip;
    }

    fn remove_node_right(&mut self) -> DeleteResult<K, D> {
        let node = self.right_node.take();
        return DeleteResult::Deleted(node);
    }

    fn height(&self) -> i32 {
        let (left, right) = (&self.left_node, &self.right_node);

        match (left, right) {
            (None, None) => 1,
            (None, Some(n)) => n.height() + 1,
            (Some(n), None) => n.height() + 1,
            (Some(l), Some(r)) => max(l.height(), r.height()) + 1,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn inserting() {
        // Empty tree must be without root
        let mut tree = BinTree::new();
        assert!(matches!(tree.root, None));

        // Inserting root
        tree.insert(0, 0);
        assert!(!matches!(tree.root, None));
        if let Some(n) = &tree.root {
            assert_eq!(n.key, 0);
            assert_eq!(n.data, 0);

            assert!(matches!(n.left_node, None));
            assert!(matches!(n.right_node, None));
        }

        // Insert on left side
        tree.insert(-1, 0);
        assert!(!matches!(tree.root, None));

        if let Some(root) = &tree.root {
            assert!(!matches!(root.left_node, None));
            assert!(matches!(root.right_node, None));

            if let Some(n) = &root.left_node {
                assert_eq!(n.key, -1);
                assert_eq!(n.data, 0);
            }
        }

        // Insert on right side
        tree.insert(1, 3);
        assert!(!matches!(tree.root, None));

        if let Some(root) = &tree.root {
            assert!(!matches!(root.left_node, None));
            assert!(!matches!(root.right_node, None));

            if let Some(n) = &root.right_node {
                assert_eq!(n.key, 1);
                assert_eq!(n.data, 3);
            }
        }
    }

    #[test]
    fn remove_last_root() {
        let mut tree = BinTree::new();
        tree.insert(0, 0);

        tree.remove(0);
        assert!(matches!(tree.root, None));
    }

    #[test]
    fn remove_leaf_near_root() {
        let mut tree = BinTree::new();

        //          0
        //         / \
        //       -2   1
        //        |
        //       -1

        tree.insert(0, 0);
        tree.insert(-2, -2);
        tree.insert(-1, -1);
        tree.insert(1, 1);

        // Remove near root
        tree.remove(1);
        assert!(!matches!(tree.root, None));

        if let Some(root) = &tree.root {
            assert!(!matches!(root.left_node, None));
            assert!(matches!(root.right_node, None));
        }

        assert!(tree.find(1) == None);
    }

    #[test]
    fn remove_leaf_far_from_root() {
        let mut tree = BinTree::new();

        //          0
        //         / \
        //       -2   1
        //        |
        //       -1

        tree.insert(0, 0);
        tree.insert(-2, -2);
        tree.insert(-1, -1);
        tree.insert(1, 1);

        // Remove in center
        tree.remove(-1);
        assert!(!matches!(tree.root, None));

        if let Some(root) = &tree.root {
            assert!(!matches!(root.left_node, None));
            assert!(!matches!(root.right_node, None));

            if let Some(n) = &root.left_node {
                assert!(matches!(n.left_node, None));
                assert!(matches!(n.right_node, None));
            }
        }

        assert!(tree.find(-1) == None);
    }

    #[test]
    fn remove_root_left_replace_one_step() {
        let mut tree = BinTree::new();

        //          0
        //         / \
        //       -2   1
        //       /
        //      -3

        tree.insert(0, 0);
        tree.insert(-2, -2);
        tree.insert(-3, -3);
        tree.insert(1, 1);

        tree.remove(0);
        assert!(!matches!(tree.root, None));

        if let Some(root) = &tree.root {
            assert_eq!(root.key, -2);

            assert!(!matches!(root.left_node, None));
            assert!(!matches!(root.right_node, None));

            if let Some(n) = &root.left_node {
                assert_eq!(n.key, -3);
            }

            if let Some(n) = &root.right_node {
                assert_eq!(n.key, 1);
            }
        }
    }

    #[test]
    fn remove_root_left_replace_many_steps() {
        let mut tree = BinTree::new();

        //          0
        //         / \
        //       -2   1
        //        |
        //       -1

        tree.insert(0, 0);
        tree.insert(-2, -2);
        tree.insert(-1, -1);
        tree.insert(1, 1);

        tree.remove(0);
        assert!(!matches!(tree.root, None));

        if let Some(root) = &tree.root {
            assert_eq!(root.key, -1);

            assert!(!matches!(root.left_node, None));
            assert!(!matches!(root.right_node, None));

            if let Some(n) = &root.left_node {
                assert_eq!(n.key, -2);
            }

            if let Some(n) = &root.right_node {
                assert_eq!(n.key, 1);
            }
        }
    }

    #[test]
    fn remove_root_right_replace() {
        let mut tree = BinTree::new();

        //      0
        //       \
        //        5
        //       / \
        //      3   7

        tree.insert(0, 0);
        tree.insert(5, 0);
        tree.insert(3, 0);
        tree.insert(7, 0);

        tree.remove(0);
        assert!(!matches!(tree.root, None));

        if let Some(root) = &tree.root {
            assert_eq!(root.key, 5);

            assert!(!matches!(root.left_node, None));
            assert!(!matches!(root.right_node, None));

            if let Some(n) = &root.left_node {
                assert_eq!(n.key, 3);
            }

            if let Some(n) = &root.right_node {
                assert_eq!(n.key, 7);
            }
        }
    }

    #[test]
    fn remove_node_left_replace_one_step() {
        let mut tree = BinTree::new();

        //      0
        //       \
        //        5
        //       / \
        //      3   7

        tree.insert(0, 0);
        tree.insert(5, 0);
        tree.insert(3, 0);
        tree.insert(7, 0);

        tree.remove(5);
        assert!(!matches!(tree.root, None));

        if let Some(root) = &tree.root {
            assert_eq!(root.key, 0);

            assert!(matches!(root.left_node, None));
            assert!(!matches!(root.right_node, None));

            if let Some(n) = &root.right_node {
                assert_eq!(n.key, 3);

                assert!(matches!(n.left_node, None));
                assert!(!matches!(n.right_node, None));

                if let Some(n) = &n.right_node {
                    assert_eq!(n.key, 7);
                }
            }
        }
    }

    #[test]
    fn remove_node_left_replace_many_steps() {
        let mut tree = BinTree::new();

        //      0
        //       \
        //        5
        //       / \
        //      2   7
        //       \
        //        4
        //       /
        //      3

        tree.insert(0, 0);
        tree.insert(5, 0);
        tree.insert(2, 0);
        tree.insert(7, 0);
        tree.insert(4, 0);
        tree.insert(3, 0);

        tree.remove(5);
        assert!(!matches!(tree.root, None));

        if let Some(root) = &tree.root {
            assert_eq!(root.key, 0);

            assert!(matches!(root.left_node, None));
            assert!(!matches!(root.right_node, None));

            if let Some(n) = &root.right_node {
                assert_eq!(n.key, 4);

                assert!(!matches!(n.left_node, None));
                assert!(!matches!(n.right_node, None));

                if let Some(n1) = &n.left_node {
                    assert_eq!(n1.key, 2);

                    assert!(matches!(n1.left_node, None));
                    assert!(!matches!(n1.right_node, None));

                    if let Some(n2) = &n1.right_node {
                        assert_eq!(n2.key, 3);
                    }
                }

                if let Some(n1) = &n.right_node {
                    assert_eq!(n1.key, 7);
                }
            }
        }
    }

    #[test]
    fn height_one_node() {
        let node = BinTreeNode::new(0, 0);
        assert_eq!(1, node.height());
    }

    #[test]
    fn height_one_side() {
        let mut root = BinTreeNode::new(0, 0);
        let mut current_node = &mut root;
        for i in 1..5 {
            current_node.right_node = Some(Box::new(BinTreeNode::new(i, i)));
            current_node = current_node.right_node.as_mut().unwrap();
        }

        assert_eq!(5, root.height());
    }

    #[test]
    fn height_left_side_higher() {
        let mut root = BinTreeNode::new(0, 0);
        let mut current_node = &mut root;
        for i in 1..3 {
            current_node.right_node = Some(Box::new(BinTreeNode::new(i, i)));
            current_node = current_node.right_node.as_mut().unwrap();
        }

        current_node = &mut root;
        for i in 1..5 {
            current_node.left_node = Some(Box::new(BinTreeNode::new(i, i)));
            current_node = current_node.left_node.as_mut().unwrap();
        }

        assert_eq!(5, root.height());
    }

    #[test]
    fn height_right_side_higher() {
        let mut root = BinTreeNode::new(0, 0);
        let mut current_node = &mut root;
        for i in 1..5 {
            current_node.right_node = Some(Box::new(BinTreeNode::new(i, i)));
            current_node = current_node.right_node.as_mut().unwrap();
        }

        current_node = &mut root;
        for i in 1..3 {
            current_node.left_node = Some(Box::new(BinTreeNode::new(i, i)));
            current_node = current_node.left_node.as_mut().unwrap();
        }

        assert_eq!(5, root.height());
    }
}
